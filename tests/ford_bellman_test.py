import unittest

from src.algorithms import FordBellman
from src.realisations import SuccessorList


class TestFordBellman(unittest.TestCase):
    def setUp(self):
        self.a = {
            's': {'t', 'y'},
            't': {'x', 'y', 'z'},
            'x': {'t'},
            'y': {'x', 'z'},
            'z': {'x', 's'}
        }

        self.weights = {
            ('s', 't'): 6,
            ('s', 'y'): 7,
            ('t', 'x'): 5,
            ('t', 'y'): 8,
            ('t', 'z'): -4,
            ('x', 't'): -2,
            ('y', 'x'): -3,
            ('y', 'z'): 9,
            ('z', 'x'): 7,
            ('z', 's'): 2
        }

        self.weight_function = lambda p: self.weights.get(
            p, float('inf')) if p[0] != p[1] else 0

        self.g = SuccessorList(self.a, self.weight_function)

    def test_shortest_path(self):
        distances, _ = FordBellman().distances_and_parents(self.g, 's')

        self.assertDictEqual({
            's': 0,
            't': 2,
            'x': 4,
            'y': 7,
            'z': -2
        }, distances)

    def test_right_path(self):
        path, _ = FordBellman().path_and_distance(self.g, 's', 'z')

        self.assertListEqual(['s', 'y', 'x', 't', 'z'], path)

    def test_connected(self):
        path, distance = FordBellman().path_and_distance(self.g, 's', 'y')

        self.assertListEqual(['s', 'y'], path)
        self.assertEqual(7, distance)
