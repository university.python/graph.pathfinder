import unittest

from src.algorithms import Contourless
from src.realisations import SuccessorList


class TestContourless(unittest.TestCase):
    def setUp(self):
        self.a = {
            1: {2},
            2: set(),
            3: {1, 2},
            4: {1, 3, 5},
            5: {1, 6},
            6: set()
        }

        self.weights = {
            (1, 2): -6,
            (3, 1): 5,
            (3, 2): -6,
            (4, 1): 10,
            (4, 3): 3,
            (4, 5): 4,
            (5, 1): -2,
            (5, 6): 7
        }

        self.weight_function = lambda p: self.weights.get(
            p, float('inf') if p[0] != p[1] else 0)

        self.g = SuccessorList(self.a, self.weight_function)

    def test_shortest_path(self):
        distances, _ = Contourless().distances_and_parents(self.g, 4)

        self.assertDictEqual({
            1: 2,
            2: -4,
            3: 3,
            4: 0,
            5: 4,
            6: 11
        }, distances)

    def test_right_path(self):
        path, _ = Contourless().path_and_distance(self.g, 5, 2)

        self.assertListEqual([5, 1, 2], path)

    def test_connected(self):
        path, distance = Contourless().path_and_distance(self.g, 4, 2)

        self.assertListEqual([4, 5, 1, 2], path)
        self.assertEqual(-4, distance)
