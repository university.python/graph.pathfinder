import unittest

from src.algorithms import Floyd
from src.realisations import SuccessorList


class TestFloyd(unittest.TestCase):
    def setUp(self):
        self.a = {
            1: {2, 3, 4},
            2: {3, 5},
            3: {4, 6},
            4: {6},
            5: set(),
            6: {3},
        }

        self.weights = {
            (1, 2): 3,
            (1, 3): 10,
            (1, 4): 1,
            (2, 3): 4,
            (2, 5): 5,
            (3, 4): 2,
            (3, 6): 12,
            (4, 6): 2,
            (6, 3): 3
        }

        self.weight_function = lambda p: self.weights.get(
            p, float('inf')) if p[0] != p[1] else 0

        self.g = SuccessorList(self.a, self.weight_function)

    def test_shortest_path(self):
        distances, _ = Floyd().distances_and_parents(self.g, 1)

        self.assertDictEqual({
            1: 0,
            2: 3,
            3: 6,
            4: 1,
            5: 8,
            6: 3
        }, distances)

    def test_right_path(self):
        path, _ = Floyd().path_and_distance(self.g, 1, 6)

        self.assertListEqual([1, 4, 6], path)

    def test_connected(self):
        path, distance = Floyd().path_and_distance(self.g, 1, 2)

        self.assertListEqual([1, 2], path)
        self.assertEqual(3, distance)
