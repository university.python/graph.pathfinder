import time
from pathfinder import Graph, Dijkstra, Contourless, FordBellman, Floyd, Pathfinder
from pathfinder.realisations import SuccessorList


def get_sample_graph() -> tuple[Graph, any, any]:
    a = {
        's': {'a', 't', 'x'},
        't': set(),
        'a': {'b', 'c'},
        'b': set(),
        'c': {'b'},
        'x': {'y', 'z'},
        'y': {'c'},
        'z': set()
    }

    weights = {
        ('s', 'a'): 3,
        ('s', 't'): 5,
        ('s', 'x'): 6,
        ('a', 'b'): 10,
        ('a', 'c'): 20,
        ('c', 'b'): 1,
        ('x', 'y'): 8,
        ('x', 'z'): 4,
        ('y', 'c'): 7
    }

    start, end = 's', 'b'

    def weight_function(e): return weights.get(
        e, float('inf') if e[0] != e[1] else 0)

    return SuccessorList(a, weight_function), start, end


def bench(pathfinder: Pathfinder, title: str):
    g, start, end = get_sample_graph()

    iterations = 1000

    start_time = time.perf_counter()

    for _ in range(1000):
        pathfinder.path_and_distance(g, start, end)

    elapsed_time = time.perf_counter() - start_time

    print(f'{title} takes {elapsed_time} to run on {iterations} iterations')


if __name__ == '__main__':
    bench(Dijkstra(), 'Dijkstra')
    bench(FordBellman(), 'FordBellman')
    bench(Floyd(), 'Floyd')
    bench(Contourless(), 'Contourless')
