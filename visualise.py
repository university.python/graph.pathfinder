import sys
from itertools import product

from PySide6 import QtCore, QtWidgets, QtGui

from src import Graph
from src.algorithms import FordBellman
from src.realisations import SuccessorList

WIDTH, HEIGHT = 600, 600


class GraphWindow(QtWidgets.QWidget):
    def __init__(self, g: Graph, logs: list):
        super().__init__()

        self.g = g
        self.logs = logs

        self.indecies = dict(zip(
            sorted(self.g.vertices()),
            range(len(self.g.vertices()))
        ))

        self.setFixedSize(WIDTH, HEIGHT)

        self.animation = QtCore.QVariantAnimation(self)
        self.animation.setDuration(10000)

        self.animation.setStartValue(0)
        self.animation.setEndValue(len(logs) - 1)

        self.animation.valueChanged.connect(self.update)
        self.animation.start()

    def paintEvent(self, event):
        pen = QtGui.QPen()

        pen.setBrush(self.palette().text().color())

        painter = QtWidgets.QStylePainter(self)

        painter.setFont(QtGui.QFont('monospaced', 13))

        self.draw_edges(pen, painter)
        self.draw_vertices(pen, painter)
        self.draw_diff(pen, painter)

    def get_vertex_position(self, vertex):
        index = self.indecies[vertex]
        part = len(self.indecies.keys())

        width_part = WIDTH / (part + 1)

        return QtCore.QPointF((index + 1) * width_part, HEIGHT / 2)

    def draw_edges(self, pen, painter):
        step = self.animation.currentValue()

        for v in self.g.vertices():
            for w in self.g.vertices():
                if v == w or self.g.get_weight(v, w) == float('inf'):
                    continue

                if self.logs[step][2].get(w, None) == v:
                    pen.setWidth(5)
                else:
                    pen.setWidth(1)

                painter.setPen(pen)

                self.draw_edge(pen, painter, v, w)

    def draw_edge(self, pen, painter, v, w):
        weight = self.g.get_weight(v, w)

        from_point = self.get_vertex_position(v)
        to_point = self.get_vertex_position(w)

        distance = to_point.x() - from_point.x()

        center = (to_point + from_point) * 0.5

        dp = QtCore.QPointF(distance / 2, distance / 2)

        a = center - dp
        b = center + dp

        if self.indecies[v] < self.indecies[w]:
            painter.drawArc(QtCore.QRectF(a, b), 180 * 16, 180 * 16)
        else:
            painter.drawArc(QtCore.QRectF(a, b), 0 * 16, 180 * 16)

        label_position = from_point + dp

        label_shift = QtCore.QPointF(-10, -7)

        painter.drawText(label_position + label_shift, str(weight))

    def draw_vertices(self, pen, painter):
        pen.setWidth(3)

        painter.setPen(pen)

        for v in self.g.vertices():
            self.draw_vertex(pen, painter, v)

    def draw_vertex(self, pen, painter, v):
        step = self.animation.currentValue()

        text = f'{v},{self.logs[step][1].get(v, float("inf"))}'

        shift = QtCore.QPointF(0, HEIGHT / 2 - 30)

        painter.drawText(self.get_vertex_position(v) + shift, text)

    def draw_diff(self, pen, painter):
        step = self.animation.currentValue()

        painter.drawText(QtCore.QPointF(WIDTH / 2, 30), str(self.logs[step][0]))


def read_succesors_list():
    vertices = input().strip().split(' ')

    succesors = dict()

    for vertex in vertices:
        succesors[vertex] = dict()

        entries = input().strip().split()

        j = 0
        while j < len(entries):
            succesors[vertex][entries[j]] = float(entries[j + 1])
            j += 2

    s = input().strip()
    t = input().strip()

    a = dict([
        (v, set(succesors[v].keys())) for v in succesors.keys()
    ])

    for v, w in product(vertices, repeat=2):
        if v == w:
            succesors[v][w] = 0
            continue

        if w not in succesors.get(v).keys():
            succesors[v][w] = float('inf')

    weight_function = lambda p: succesors[p[0]][p[1]]

    return SuccessorList(a, weight_function), s, t


def main():
    g, s, t = read_succesors_list()
    algo = FordBellman()

    algo.distances_and_parents(g, s)

    logs = list(algo.get_logs(s))
    logs = [(s, {}, {})] + logs
    logs = logs + [(t, logs[-1][1], logs[-1][2])]

    app = QtWidgets.QApplication([])

    widget = GraphWindow(g, logs)
    widget.resize(600, 600)
    widget.show()

    sys.exit(app.exec())


if __name__ == "__main__":
    main()
