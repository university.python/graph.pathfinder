from itertools import product

from src import Graph, Pathfinder, StepLogger


class Floyd(Pathfinder, StepLogger):
    def __compute_distances_and_parents(self, g: Graph) -> tuple[dict[(any, any), float], dict[(any, any), any]]:
        distances = dict([
            ((i, j), g.get_weight(i, j))
            for i, j in product(g.vertices(), repeat=2)
        ])

        parents = dict([
            ((i, j), i)
            for i, j in product(g.vertices(), repeat=2)
        ])

        self.start_log(None, distances, parents)

        for m, i, j in product(g.vertices(), repeat=3):
            through_m = distances[(i, m)] + distances[(m, j)]

            if through_m < distances[(i, j)]:
                distances[(i, j)] = through_m
                parents[(i, j)] = parents[(m, j)]

                self.append_log((i, j), distances, parents)

        return distances, parents

    def distances_and_parents(self, g: Graph, start) -> tuple[dict[any, float], dict]:
        distances, parents = self.__compute_distances_and_parents(g)

        d = dict([
            (end, distance) for (s, end), distance in distances.items()
            if start == s
        ])

        p = dict([
            (end, parent) for (s, end), parent in parents.items()
            if start == s
        ])

        return d, p

    def get_logs(self, s=None):
        logs = super().get_logs()

        for (p, distance, parents) in logs:
            p_d = dict([
                (v, d) for ((start, v), d) in distance.items()
                if start == s
            ])

            p_p = dict([
                (v, p) for ((start, v), p) in parents.items()
                if start == s
            ])

            if p is None:
                yield s, p_d, p_p
            elif p[0] == s:
                yield p[1], p_d, p_p
