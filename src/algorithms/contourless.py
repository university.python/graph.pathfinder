from itertools import product

from src import Graph, Pathfinder, Validator, StepLogger


class Contourless(Pathfinder, Validator, StepLogger):
    """
    Requirements: graph doesn't contain contours.
    """
    def __topological_sort(self, g: Graph) -> dict[any, int]:
        degrees = dict([
            (v, len(g.get_successors(v)))
            for v in g.vertices()
        ])

        stack = [
            v for v in g.vertices()
            if degrees[v] == 0
        ]

        indecies = dict()

        for i in range(0, len(g.vertices())):
            v = stack.pop()
            indecies[v] = len(g.vertices()) - 1 - i

            for w in g.get_ancestors(v):
                degrees[w] -= 1

                if degrees[w] == 0:
                    stack.append(w)

        return indecies

    def distances_and_parents(self, g: Graph, start) -> tuple[dict[any, float], dict]:
        indecies = self.__topological_sort(g)

        vertecies = dict([
            (i, v) for (v, i) in indecies.items()
        ])

        distances = dict([
            (v, g.get_weight(start, v))
            for v in g.vertices()
        ])

        parents = dict([
            (v, start) for v in g.vertices()
            if g.are_connected(start, v)
        ])

        self.start_log(start, distances, parents)

        for i in range(indecies[start] + 1, len(g.vertices())):
            v = vertecies[i]

            for w in g.get_ancestors(v):
                through_w = distances[w] + g.get_weight(w, v)

                if through_w < distances[v]:
                    distances[v] = through_w
                    parents[v] = w

                    self.append_log(v, distances, parents)

        return distances, parents

    def is_valid(self, g: Graph) -> bool:
        indecies = self.__topological_sort(g)

        for v, w in product(g.vertices(), repeat=2):
            if g.are_connected(v, w) and indecies[v] > indecies[w]:
                return False

        return True
