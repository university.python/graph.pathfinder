from itertools import product

from src import Graph, Pathfinder, Validator, StepLogger


class FordBellman(Pathfinder, Validator, StepLogger):
    """
    Requirements: graph doesn't contain contours with length < 0
    """
    def distances_and_parents(self, g: Graph, start) -> tuple[dict[any, float], dict]:
        distances: dict[any, float] = dict([
            (v, g.get_weight(start, v))
            for v in g.vertices()
        ])

        parents: dict[any, any] = dict([
            (v, start) for v in g.vertices()
            if g.are_connected(start, v)
        ])

        self.start_log(start, distances, parents)

        for _, v, w in product(g.vertices(), repeat=3):
            through_w = distances[w] + g.get_weight(w, v)

            if through_w < distances[v]:
                distances[v] = through_w
                parents[v] = w

                self.append_log(v, distances, parents)

        return distances, parents

    def is_valid(self, g: Graph) -> bool:
        start = g.vertices()[0]

        distances, _ = self.distances_and_parents(g, start)

        for v, w in product(g.vertices(), repeat=2):
            through_w = distances[w] + g.get_weight(w, v)

            if through_w < distances[v]:
                return False

        return True
