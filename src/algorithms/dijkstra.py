from itertools import product

from src import Graph
from src import Pathfinder, Validator, StepLogger


class Dijkstra(Pathfinder, Validator, StepLogger):
    """
    Requirements: graph doesn't contain edges with weight < 0.
    """
    def distances_and_parents(self, g: Graph, start) -> tuple[dict[any, float], dict]:
        distances = dict([
            (v, g.get_weight(start, v))
            for v in g.vertices()
        ])

        parents = dict([
            (v, start) for v in g.vertices()
            if g.are_connected(start, v)
        ])

        left = g.vertices() - {start}

        self.start_log(start, distances, parents)

        while len(left) > 0:
            nearest = min(left, key=lambda x: distances[x])

            left.remove(nearest)

            for v in left:
                through_nearest = distances[nearest] + g.get_weight(nearest, v)

                if through_nearest < distances[v]:
                    distances[v] = through_nearest
                    parents[v] = nearest

                    self.append_log(v, distances, parents)

        return distances, parents

    def is_valid(self, g: Graph) -> bool:
        for v, w in product(g.vertices(), repeat=2):
            if g.get_weight(v, w) < 0:
                return False

        return True
