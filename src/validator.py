from abc import ABC, abstractmethod

from src import Graph


class Validator(ABC):
    """
    This class is used to validate specific requirements of your algorithm.
    """
    @abstractmethod
    def is_valid(self, g: Graph) -> bool:
        """
        :param g: oriented
        :return: **True** if `g` valid for your requirements, otherwise **False**
        """
        return True
