from abc import ABC
from itertools import pairwise, chain


class StepLogger(ABC):
    def __init__(self):
        self.__logs = []

    def start_log(self, v, distances, parents):
        self.append_log(v, distances, parents)

    def append_log(self, v, distances, parents):
        self.__logs.append((v, distances.copy(), parents.copy()))

    def get_logs(self, s=None):
        yield from self.__logs

    def get_differences(self, s=None):
        logs = chain([(None, dict(), dict())], self.get_logs(s))

        def difference(dict1, dict2):
            return dict(set(dict2.items()) - set(dict1.items()))

        for ((v1, d1, p1), (v2, d2, p2)) in pairwise(logs):
            distance_difference = difference(d1, d2)
            parents_difference = difference(p1, p2)

            yield v2, distance_difference, parents_difference
