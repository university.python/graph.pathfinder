from src.graph import Graph


class SuccessorList(Graph):
    """
    Represents oriented graph as a successor list.
    """
    def __init__(self, adjacency_list: dict[any, set[any]], weight_function) -> None:
        super().__init__()

        self.successors = adjacency_list
        self.weight_function = weight_function

        self.ancestors = None

    def vertices(self) -> set:
        return set(self.successors.keys())

    def get_weight(self, start, end) -> float:
        return self.weight_function((start, end))

    def get_successors(self, start) -> set:
        return self.successors[start]

    def __calculate_ancestors(self) -> set:
        ancestors: dict[any, set] = dict([
            (v, set()) for v in self.vertices()
        ])

        for end, neighbours in self.successors.items():
            for start in neighbours:
                ancestors[start].add(end)

        return ancestors

    def get_ancestors(self, end) -> set:
        if self.ancestors is None:
            self.ancestors = self.__calculate_ancestors()

        return self.ancestors[end]
