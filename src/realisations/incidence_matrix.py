from src.graph import Graph


class IncidenceMatrix(Graph):
    """
    Represents oriented graph as an incidence matrix.
    """
    def __init__(self, size: int, incidence_matrix: list[list[float]]) -> None:
        super().__init__()

        self.size = size
        self.incidence_matrix = incidence_matrix

    def vertices(self) -> set:
        return set(range(self.size))

    def get_weight(self, start, end) -> float:
        return self.incidence_matrix[start][end]
