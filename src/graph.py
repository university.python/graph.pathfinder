from abc import ABC, abstractmethod


class Graph(ABC):
    """
    Abstract oriented graph representation.
    """
    @abstractmethod
    def vertices(self) -> set:
        """
        :return: vertices set of graph
        """
        raise NotImplementedError()

    @abstractmethod
    def get_weight(self, start, end) -> float:
        """
        :param start: vertex of oriented graph
        :param end: vertex of oriented graph
        :return: weight of oriented edge (`start`, `end`) or `inf`, if edge does not exist
        """
        raise NotImplementedError()

    def get_successors(self, start) -> set:
        """
        :param start: vertex of oriented graph
        :return: set of successors of `start` vertex
        """
        return set([
            end for end in self.vertices()
            if self.are_connected(start, end)
        ])

    def get_ancestors(self, end) -> set:
        """
        :param end: vertex of oriented graph
        :return: set of ancestors of `end` vertex
        """
        return set([
            start for start in self.vertices()
            if self.are_connected(start, end)
        ])

    def are_connected(self, start, end) -> bool:
        """
        :param start: vertex of oriented graph
        :param end: vertex of oriented graph
        :return: **True**, if oriented edge (`start`, `end`) exists, otherwise **False**
        """
        return self.get_weight(start, end) != float('inf')
