from abc import ABC, abstractmethod

from src import Graph


class Pathfinder(ABC):
    """
    Abstract representation of path finding graph algorithm.
    """
    @abstractmethod
    def distances_and_parents(self, g: Graph, start) -> tuple[dict[any, float], dict]:
        """
        :param g: oriented graph
        :param start: first vertex of result path
        :return:
        [0] Key: vertex name, Value: distance from `start` to `key`.
        [1] Key: vertex name, Value: last vertex before `key` on the shortest `start` -> `key` path
        """
        raise NotImplementedError()

    def path_and_distance(self, g: Graph, start, end) -> tuple[list, float] | None:
        """
        :param g: oriented graph
        :param start: first vertex of result path
        :param end: target vertex
        :return: list of vertices, that represents the shortest `start` -> `end` path and length of that path
        """
        distances, parents = self.distances_and_parents(g, start)

        if distances[end] == float('inf'):
            return None

        path = []

        current = end
        while parents[current] != current:
            path.append(current)
            current = parents[current]

        path.append(start)

        path.reverse()

        return path, distances[end]
